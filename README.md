# Data Path Layout (DPL) files

This repo contains differnt DPL files that can be loaded into the DPAA2 Management Complex (MC) during system
boot to configure the DPAA2 resources. Alternatively the resources can be modified at runtime by the Linux 
kernel drivers or userspace tools like `restool`.

DPL files are specified in a device tree .dts file. The dts file can be compiled into a binary representation using
the device tree compiler `dtc` with the following command:

```
dtc -I dts -O dtb -o custom-dpl.dtb custom-dpl.dts
```

## Rule of thumb for adding NICs

If you want to add a NIC (aka dpni in DPAA2 terminology) we need at least the following supporting resources in the same
dprc container:

 * 1           x dpbp
 * 1           x dpmcp ( + 1 dpmcp if you have a dpmac in the same container)
 * $num_queues x dpcon (where $num_queues is an option for the dpni that defaults to 1)
 * $#cores     x dpio  (where $#cores is the number of processor cores available. With decreased performance also fewer dpio's work)
 * ?           x dpmac (If the NIC should connect to a wire, we will need a dpmac. In that case don't forget an extra dpmcp; see above)  

## Write dpl dtb to Ten64 flash

In the normal boot process of the Ten64, the dpl is read from NAND partition 4 (/dev/mtd4, label=dpl). To write a custom
dpl to this partition, we can stop the autoboot and use the u-boot shell. Under the assumption that the custom dpl device
tree binary is stored on the USB key on partition 2 under the path `root/eth-dpl-custom.dtb`, we can use the following 
command to write the dpl into the NAND memory:

```
usb start; ext4load usb 0:2 $load_addr "root/eth-dpl-custom.dtb"; mtd erase dpl; mtd write dpl $load_addr +$filesize
```
