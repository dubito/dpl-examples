/dts-v1/;
/ {
	dpl-version = <10>;
	/*****************************************************************
	 * Containers
	 *****************************************************************/
	containers {

		dprc@1 {
			compatible = "fsl,dprc";
			parent = "none";
			options = "DPRC_CFG_OPT_SPAWN_ALLOWED", "DPRC_CFG_OPT_ALLOC_ALLOWED", "DPRC_CFG_OPT_OBJ_CREATE_ALLOWED", "DPRC_CFG_OPT_TOPOLOGY_CHANGES_ALLOWED", "DPRC_CFG_OPT_IRQ_CFG_ALLOWED";

			objects {

				obj_set@dpbp {
					type = "dpbp";
					ids = <5 >;
				};

				obj_set@dpcon {
					type = "dpcon";
					ids = <16 >;
				};

				obj_set@dpio {
					type = "dpio";
					ids = <8 >;
				};

				obj_set@dpmac {
					type = "dpmac";
					ids = <3 4 5 6 7 8 9 10 >;
				};

				obj_set@dpmcp {
					type = "dpmcp";
					ids = <13 14 15 16 17 18 19 20 21 22>;
				};

				obj_set@dpni {
					type = "dpni";
					ids = <4 >;
				};

				obj_set@dprtc {
					type = "dprtc";
					ids = <0 >;
				};

                obj_set@dpdmux {
					type = "dpdmux";
					ids = <0 >;
				};
			};
		};

		dprc@2 {
            /* Container for the router: One WAN interface (dpni.0 <-> dpmac.8) and one LAN interface (dpni.1) connected to the switch */
			compatible = "fsl,dprc";
			parent = "dprc@1";
			options = "DPRC_CFG_OPT_SPAWN_ALLOWED", "DPRC_CFG_OPT_ALLOC_ALLOWED", "DPRC_CFG_OPT_OBJ_CREATE_ALLOWED", "DPRC_CFG_OPT_IRQ_CFG_ALLOWED";

			objects {

				obj_set@dpbp {
					type = "dpbp";
					ids = <0 1 2 3 7 8 9 10>;
				};

				obj_set@dpcon {
					type = "dpcon";
					ids = <0 1 2 3 4 5 6 7 8 9 10 11 12 20 21 22 23 24 25 26 27 28 29 30 31>;
				};

				obj_set@dpio {
					type = "dpio";
					ids = <1 2 3 4 >;
				};

				obj_set@dpmcp {
					type = "dpmcp";
					ids = <1 2 3 4 5 6 7 8 25 26 27 28>;
				};

				obj_set@dpni {
					type = "dpni";
					ids = <0 1 2 3 7 8 9 10>;
				};

				obj_set@dpseci {
					type = "dpseci";
					ids = <1 >;
				};
			};
		};

        dprc@3 {
            /* Container for the application VM: One LAN interface (dpni.5) connected to the switch */
			compatible = "fsl,dprc";
			parent = "dprc@1";
			options = "DPRC_CFG_OPT_SPAWN_ALLOWED", "DPRC_CFG_OPT_ALLOC_ALLOWED", "DPRC_CFG_OPT_OBJ_CREATE_ALLOWED", "DPRC_CFG_OPT_IRQ_CFG_ALLOWED";

			objects {

				obj_set@dpbp {
					type = "dpbp";
					ids = <4 >;
				};

				obj_set@dpcon {
					type = "dpcon";
					ids = <13 14 15 >;
				};

				obj_set@dpio {
					type = "dpio";
					ids = <5 6 >;
				};

				obj_set@dpmcp {
					type = "dpmcp";
					ids = <9 10 11 12 >;
				};

				obj_set@dpni {
					type = "dpni";
					ids = <5 >;
				};

				obj_set@dpseci {
					type = "dpseci";
					ids = <0 >;
				};
			};
		};

		dprc@4 {
            /* Container for the storage VM: One LAN interface (dpni.6) connected to the switch */
			compatible = "fsl,dprc";
			parent = "dprc@1";
			options = "DPRC_CFG_OPT_SPAWN_ALLOWED", "DPRC_CFG_OPT_ALLOC_ALLOWED", "DPRC_CFG_OPT_OBJ_CREATE_ALLOWED", "DPRC_CFG_OPT_IRQ_CFG_ALLOWED";

			objects {

				obj_set@dpbp {
					type = "dpbp";
					ids = <6 >;
				};

				obj_set@dpcon {
					type = "dpcon";
					ids = <17 18 19 >;
				};

				obj_set@dpio {
					type = "dpio";
					ids = <7 >;
				};

				obj_set@dpmcp {
					type = "dpmcp";
					ids = <23 24 >;
				};

				obj_set@dpni {
					type = "dpni";
					ids = <6 >;
				};
			};
		};
	};

	/*****************************************************************
	 * Objects
	 *****************************************************************/
	objects {

		dpbp@0 {
			compatible = "fsl,dpbp";
		};

		dpbp@1 {
			compatible = "fsl,dpbp";
		};

		dpbp@2 {
			compatible = "fsl,dpbp";
		};

		dpbp@3 {
			compatible = "fsl,dpbp";
		};

		dpbp@4 {
			compatible = "fsl,dpbp";
		};

		dpbp@5 {
			compatible = "fsl,dpbp";
		};

		dpbp@6 {
			compatible = "fsl,dpbp";
		};

		dpbp@7 {
			compatible = "fsl,dpbp";
		};

		dpbp@8 {
			compatible = "fsl,dpbp";
		};

		dpbp@9 {
			compatible = "fsl,dpbp";
		};

		dpbp@10 {
			compatible = "fsl,dpbp";
		};

		dpcon@0 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@1 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@2 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@3 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@4 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@5 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@6 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@7 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@8 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@9 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@10 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@11 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@12 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@13 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@14 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@15 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@16 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@17 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@18 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};
		
		dpcon@19 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@20 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@21 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@22 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@23 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@24 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@25 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@26 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@27 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@28 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@29 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@30 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpcon@31 {
			compatible = "fsl,dpcon";
			num_priorities = <0x2>;
		};

		dpdmux@0 {
		    compatible = "fsl,dpdmux";
		    options = "DPDMUX_OPT_BRIDGE_EN";
		    method = "DPDMUX_METHOD_MAC";
		    manip = "DPDMUX_MANIP_NONE";
		    control_if = <0>;
		    num_ifs = <5>;
		    max_dmat_entries = <24>;
		    max_mc_groups = <12>;
		};

		dpio@1 {
			compatible = "fsl,dpio";
			channel_mode = "DPIO_LOCAL_CHANNEL";
			num_priorities = <0x8>;
		};

		dpio@2 {
			compatible = "fsl,dpio";
			channel_mode = "DPIO_LOCAL_CHANNEL";
			num_priorities = <0x8>;
		};

		dpio@3 {
			compatible = "fsl,dpio";
			channel_mode = "DPIO_LOCAL_CHANNEL";
			num_priorities = <0x8>;
		};

		dpio@4 {
			compatible = "fsl,dpio";
			channel_mode = "DPIO_LOCAL_CHANNEL";
			num_priorities = <0x8>;
		};

		dpio@5 {
			compatible = "fsl,dpio";
			channel_mode = "DPIO_LOCAL_CHANNEL";
			num_priorities = <0x8>;
		};

		dpio@6 {
			compatible = "fsl,dpio";
			channel_mode = "DPIO_LOCAL_CHANNEL";
			num_priorities = <0x8>;
		};

		dpio@7 {
			compatible = "fsl,dpio";
			channel_mode = "DPIO_LOCAL_CHANNEL";
			num_priorities = <0x8>;
		};

		dpio@8 {
			compatible = "fsl,dpio";
			channel_mode = "DPIO_LOCAL_CHANNEL";
			num_priorities = <0x8>;
		};

		dpmac@3 {
			compatible = "fsl,dpmac";
		};

		dpmac@4 {
			compatible = "fsl,dpmac";
		};

		dpmac@5 {
			compatible = "fsl,dpmac";
		};

		dpmac@6 {
			compatible = "fsl,dpmac";
		};

		dpmac@7 {
			compatible = "fsl,dpmac";
		};

		dpmac@8 {
			compatible = "fsl,dpmac";
		};

		dpmac@9 {
			compatible = "fsl,dpmac";
		};

		dpmac@10 {
			compatible = "fsl,dpmac";
		};

		dpmcp@1 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@2 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@3 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@4 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@5 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@6 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@7 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@8 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@9 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@10 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@11 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@12 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@13 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@14 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@15 {
			compatible = "fsl,dpmcp";
		};

        dpmcp@16 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@17 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@18 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@19 {
			compatible = "fsl,dpmcp";
		};

        dpmcp@20 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@21 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@22 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@23 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@24 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@25 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@26 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@27 {
			compatible = "fsl,dpmcp";
		};

		dpmcp@28 {
			compatible = "fsl,dpmcp";
		};

        /* Router WAN */
		dpni@0 {
			compatible = "fsl,dpni";
			type = "DPNI_TYPE_NIC";
			num_queues = <1>;
			num_tcs = <1>;
			num_cgs = <1>;
			mac_filter_entries = <16>;
			vlan_filter_entries = <0>;
			fs_entries = <32>;
			qos_entries = <0>;
		};

        /* Router LAN*/
		dpni@1 {
			compatible = "fsl,dpni";
			type = "DPNI_TYPE_NIC";
			num_queues = <4>;
			num_tcs = <1>;
			num_cgs = <1>;
			mac_filter_entries = <16>;
			vlan_filter_entries = <0>;
			fs_entries = <32>;
			qos_entries = <0>;
		};

        /* GbE LAN port*/
		dpni@2 {
			compatible = "fsl,dpni";
			type = "DPNI_TYPE_NIC";
			num_queues = <4>;
			num_tcs = <1>;
			num_cgs = <1>;
			mac_filter_entries = <16>;
			vlan_filter_entries = <0>;
			fs_entries = <1>;
			qos_entries = <0>;
		};

        /* GbE LAN port*/
		dpni@3 {
			compatible = "fsl,dpni";
			type = "DPNI_TYPE_NIC";
			num_queues = <4>;
			num_tcs = <1>;
			num_cgs = <1>;
			mac_filter_entries = <16>;
			vlan_filter_entries = <0>;
			fs_entries = <1>;
			qos_entries = <0>;
		};

        /* Host LAN */
		dpni@4 {
			compatible = "fsl,dpni";
			type = "DPNI_TYPE_NIC";
			num_queues = <1>;
			num_tcs = <1>;
			num_cgs = <1>;
			mac_filter_entries = <16>;
			vlan_filter_entries = <0>;
			fs_entries = <1>;
			qos_entries = <0>;
		};

		/* Application VM LAN*/
		dpni@5 {
			compatible = "fsl,dpni";
			type = "DPNI_TYPE_NIC";
			num_queues = <3>;
			num_tcs = <1>;
			num_cgs = <1>;
			mac_filter_entries = <16>;
			vlan_filter_entries = <0>;
			fs_entries = <1>;
			qos_entries = <0>;
		};

        /* Storage VM LAN */
		dpni@6 {
			compatible = "fsl,dpni";
			type = "DPNI_TYPE_NIC";
			num_queues = <3>;
			num_tcs = <1>;
			num_cgs = <1>;
			mac_filter_entries = <16>;
			vlan_filter_entries = <0>;
			fs_entries = <1>;
			qos_entries = <0>;
		};

		/* GbE LAN port*/
		dpni@7 {
			compatible = "fsl,dpni";
			type = "DPNI_TYPE_NIC";
			num_queues = <3>;
			num_tcs = <1>;
			num_cgs = <1>;
			mac_filter_entries = <16>;
			vlan_filter_entries = <0>;
			fs_entries = <1>;
			qos_entries = <0>;
		};

		/* GbE LAN port*/
		dpni@8 {
			compatible = "fsl,dpni";
			type = "DPNI_TYPE_NIC";
			num_queues = <3>;
			num_tcs = <1>;
			num_cgs = <1>;
			mac_filter_entries = <16>;
			vlan_filter_entries = <0>;
			fs_entries = <1>;
			qos_entries = <0>;
		};

		/* GbE LAN port*/
		dpni@9 {
			compatible = "fsl,dpni";
			type = "DPNI_TYPE_NIC";
			num_queues = <3>;
			num_tcs = <1>;
			num_cgs = <1>;
			mac_filter_entries = <16>;
			vlan_filter_entries = <0>;
			fs_entries = <1>;
			qos_entries = <0>;
		};

		/* GbE LAN port*/
		dpni@10 {
			compatible = "fsl,dpni";
			type = "DPNI_TYPE_NIC";
			num_queues = <3>;
			num_tcs = <1>;
			num_cgs = <1>;
			mac_filter_entries = <16>;
			vlan_filter_entries = <0>;
			fs_entries = <1>;
			qos_entries = <0>;
		};
		dprtc@0 {
			compatible = "fsl,dprtc";
		};

		dpseci@0 {
			compatible = "fsl,dpseci";
			num_queues = <2>;
			priorities = <2 4 >;
		};

		dpseci@1 {
			compatible = "fsl,dpseci";
			num_queues = <2>;
			priorities = <2 4 >;
		};
	};

	/*****************************************************************
	 * Connections
	 *****************************************************************/
	connections {

        /*Connect GE1 (upper left) to WAN Port*/
		connection@1{
			endpoint1 = "dpni@0";
			endpoint2 = "dpmac@8";
		};

		/*Connect bridge upstream to GE1 (lower left)*/
		connection@2{
			endpoint1 = "dpdmux@0/if@0";
			endpoint2 = "dpmac@7";
		};

		/*Connect router VM to bridge*/
        connection@3{
			endpoint1 = "dpdmux@0/if@1";
			endpoint2 = "dpni@1";
		};

		/*Connect host to bridge*/
        connection@4{
			endpoint1 = "dpdmux@0/if@2";
			endpoint2 = "dpni@4";
		};

		/*Connect app VM to bridge*/
        connection@5{
			endpoint1 = "dpdmux@0/if@3";
			endpoint2 = "dpni@5";
		};

		/*Connect storage VM to bridge*/
		connection@6{
			endpoint1 = "dpdmux@0/if@4";
			endpoint2 = "dpni@6";
		};

		/*Connect LAN port for software bridging in router VM*/
        connection@7{
			endpoint1 = "dpni@2";
			endpoint2 = "dpmac@9";
		};

		/*Connect LAN port for software bridging in router VM*/
        connection@8{
			endpoint1 = "dpni@3";
			endpoint2 = "dpmac@10";
		};

		/*Connect LAN port for software bridging in router VM*/
        connection@9{
			endpoint1 = "dpni@7";
			endpoint2 = "dpmac@3";
		};

		/*Connect LAN port for software bridging in router VM*/
        connection@10{
			endpoint1 = "dpni@8";
			endpoint2 = "dpmac@4";
		};

		/*Connect LAN port for software bridging in router VM*/
        connection@11{
			endpoint1 = "dpni@9";
			endpoint2 = "dpmac@5";
		};

		/*Connect LAN port for software bridging in router VM*/
        connection@12{
			endpoint1 = "dpni@10";
			endpoint2 = "dpmac@6";
		};
	};
};
